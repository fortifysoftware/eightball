################################################################################################################################################ 
# Fortify lets you build secure software fast with an appsec platform that automates testing throughout the DevSecOps pipeline. Fortify static,#
# dynamic, interactive, and runtime security testing is available on premises or as a service. To learn more about Fortify, start a free trial #
# or contact our sales team, visit microfocus.com/appsecurity.                                                                                 #
#                                                                                                                                              #
# Use this pipeline template as a basis for integrating Fortify ScanCentral into your GitLab pipelines to perform Static Application Security  #
# Testing (SAST). This template demonstrates the steps to prepare the code+dependencies and initiate a scan. Users should review inputs and    #
# environment variables below to configure scanning for an existing application in Fortify Software Security Center. Additional information is #
# available in the comments throughout the template and the Fortify ScanCentral product documentation.                                         #
################################################################################################################################################

fortify-sast-scancentral:
  image: fortifydocker/fortify-ci-tools:latest

  # Update/override variables based on the ScanCentral Client documentation for your project's included tech stack(s). Helpful hints:
  #   ScanCentral Client will download dependencies for maven (-bt mvn) and gradle (-bt gradle).
  #   The current fortify-ci-tools image is Linux only at this time. MSBuild integration is not currently supported.
  #   ScanCentral has additional options that should be set for PHP and Python projects.
  #   For other build tools (-bt none), add your build commands to download necessary dependencies.
  #   ScanCentral documentation is located at https://www.microfocus.com/documentation/fortify-software-security-center/ 
  variables:
    SCANCENTRAL_BUILD_OPTS: "-bt none"
    # SSC_APP: $CI_PROJECT_NAME
    # SSC_PV: $CI_COMMIT_REF_NAME

  # Run Fortify ScanCentral Client. SC_URL and SSC_TOKEN are expected as GitLab CICD Variables in the template (masking recommended).
  script:
    - 'ls -la /opt/Fortify/'
    - 'echo Set client auth token'
    - 'sed -ir "s/^[#]*\s*${CLIENT_PK}=.*/$CLIENT_PK=$CLIENT_AUTH_TOKEN/" /opt/Fortify/ScanCentral/Core/config/client.properties'
    - 'echo Run scancentral job'
    - 'scancentral -url $SC_URL start $SCANCENTRAL_BUILD_OPTS -upload -application $SSC_APP -version $SSC_PV -uptoken $SSC_TOKEN'
  
  # Check status of scan
    - |
      FORTIFYTOKEN=$(echo -n "$SSC_TOKEN" | base64)
      JOB_TOKEN="$(grep -R "Submitted job and received token" ~/.fortify/scancentral* | tail -1 | grep -E -o '[-0-9a-z]{36}$')"

      # Poll period (in seconds) to check job status
      POLL_PERIOD="5s"

      # Wait until JOB_TOKEN shows up in list of cloud jobs
      while [ -z "$RESPONSE" ] || [ -z "$(echo $RESPONSE | grep -oh '"responseCode":200')" ]
      do
      	RESPONSE=$(curl -s -w '\n' ${SSC_URL}/api/v1/cloudjobs/$JOB_TOKEN -H 'Accept: application/json' -H 'Authorization: FortifyToken '"$FORTIFYTOKEN")
        echo RESPONSE=$RESPONSE
        if [ ! -z "$(echo $RESPONSE | grep -oh 'Access Denied. Unauthorized access')" ]; then
          echo "Access Denied. Unauthorized access. Token might be invalid. Exiting with error."
        exit 1
        fi
        sleep $POLL_PERIOD
      done

      PV_ID=$(echo $RESPONSE | grep -E -o '\"pvId\":[0-9]*' | grep -o '[0-9]*')
      echo PV_ID=$PV_ID

      JOB_STATE=$(echo $RESPONSE | grep -E -o '\"jobState\":\"[_A-Z]*\"' | grep -E -o '[_A-Z]{2,}')
      echo JOB_STATE=$JOB_STATE

      # Wait until scan job is completed
      while [ "$JOB_STATE" = "PENDING" ] || [ "$JOB_STATE" = "SCAN_RUNNING" ] || [ "$JOB_STATE" = "UPLOAD_QUEUED" ]
      do
      	sleep $POLL_PERIOD
        RESPONSE=$(curl -s -w '\n'  ${SSC_URL}/api/v1/cloudjobs/$JOB_TOKEN -H 'Accept: application/json' -H 'Authorization: FortifyToken '"$FORTIFYTOKEN")
        JOB_STATE=$(echo $RESPONSE | grep -E -o '\"jobState\":\"[_A-Z]*\"' | grep -E -o '[_A-Z]{2,}')
      done

      if [ ! "$JOB_STATE" = "UPLOAD_COMPLETED" ]
      then
      	echo ScanCentral job completed but artifact upload either failed or canceled. Setting build status to failed.
        exit 1
      fi

      RESPONSE=$(curl -s -w '\n'  ${SSC_URL}/api/v1/projectVersions/$PV_ID/artifacts?limit=1 -H 'Accept: application/json' -H 'Authorization: FortifyToken '"$FORTIFYTOKEN")

      ARTIFACT_STATUS=$(echo $RESPONSE | grep -E -o '\"status\":\"[_A-Z]*\"' | grep -E -o '[_A-Z]{2,}')
      echo ARTIFACT_STATUS=$ARTIFACT_STATUS

      # Wait until artifact completes processing
      if [ "$ARTIFACT_STATUS" = "PROCESSING" ] || [ "$ARTIFACT_STATUS" = "SCHED_PROCESSING" ]
      then
        sleep $POLL_PERIOD
        RESPONSE=$(curl -s -w '\n'  ${SSC_URL}/api/v1/projectVersions/$PV_ID/artifacts?limit=1 -H 'Accept: application/json' -H 'Authorization: FortifyToken '"$FORTIFYTOKEN")
        ARTIFACT_STATUS=$(echo $RESPONSE | grep -E -o '\"status\":\"[_A-Z]*\"' | grep -E -o '[_A-Z]{2,}')
      fi

      if [ ! "$ARTIFACT_STATUS" = "PROCESS_COMPLETE" ]
      then
      	echo The ScanCentral job completed but artifact upload failed processing.
        exit 1
      fi

      # Use the FortifySecurityRating performance indicator to determine if any critical or high severity issues were detected
      echo ARTIFACT_STATUS=$ARTIFACT_STATUS
      RESPONSE=$(curl -s -w '\n'  ${SSC_URL}/api/v1/projectVersions/$PV_ID/performanceIndicatorHistories/FortifySecurityRating -H  'Accept: application/json' -H  'Authorization: FortifyToken '"$FORTIFYTOKEN")
      echo PI RESPONSE=$RESPONSE
      RATING=$(echo $RESPONSE | grep -E -o '\"value\":[0-9]\.[0-9]' | cut -b 9)
      echo RATING=$RATING

      if [ "$RATING" -lt "3" ]
      then
      	echo Critical or High severity issues detected. Failing build.
        exit 1
      else
      	echo No Critical or High severity issues detected. Passing build.
        exit
      fi
  
  allow_failure: true